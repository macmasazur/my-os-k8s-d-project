package tst;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Generate {

	public static Integer[] input = new Integer[]{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70};
	
	// create a list of Integer type
	public static List<Integer> list = new ArrayList<>(Arrays.asList(input));
	public static List<Integer> pattern = new ArrayList<>(Arrays.asList(input));

	// Drive Function
	public static void main(String[] args) {

		Generate obj = new Generate();

		// boundIndex for select in sub list
		int numberOfElements = 7;

		// take a random element from list and print them
		obj.getRandomElement(list, numberOfElements, 10);
	}

	// Function select an element base on index and return
	// an element
	public List<Integer> getRandomElement(List<Integer> list, int totalItems, int rows) {
		Random rand = new Random();

		// create a temporary list for storing
		// selected element
		List<Integer> newList = new ArrayList<>();
		for (int j = 0; j < rows; j++) {
			for (int i = 0; i < totalItems; i++) {
	
				// take a raundom index between 0 to size
				// of given List
				int randomIndex = rand.nextInt(list.size());
	
				// add element in temporary list
				newList.add(list.get(randomIndex));
	
				// Remove selected element from orginal list
				list.remove(randomIndex);
			}
			
			Collections.sort(newList);
			System.out.println(newList);
			newList.clear();
		}
		return newList;
	}
}
